import xlrd
import pandas as pd
import csv

#Leitura da lista de nomes de dispositivos e criação do arquivo CSV para armazenar o resultado da coleta
def ler_entrada():
    dados_dispositivos = pd.read_excel('Handsets.xlsx')
    
    with open('dados_coletados.csv', 'w', newline='') as arquivo_csv:
        colunas = ['Handset', 'Nome Comercial' ,'Marca', 'Modelo', 'Capacidade da Bateria (mAh)', 'Memória RAM(GB)', 'Capacidade de Armazenamento(GB)', 'Versão do Bluetooth', 'NFC', 
        'Dual Chip', 'LTE', 'Resolução da Câmera Traseira(Mpx)', 'Resolução da Câmera Traseira(Mpx)', 'Dimensões (Largura, Altura, Espessura)', 'Peso(g)', 
        'Tamanho (Polegadas)', 'Sistema Operacional', 'Versão SO', 'Tipo do Processador', 'Velocidade do Processador', 'URL da Coleta', 'Data da Coleta', 
        'Ano de Lançamento', 'Preço',  'Avaliação do Site', 'Avaliação dos Usuários']
        writer = csv.DictWriter(arquivo_csv, fieldnames=colunas)
        writer.writeheader()
    arquivo_csv.close()

    return dados_dispositivos


#Gravação dos dados coletados de um dispositivo em arquivo csv, após feita a análise. Se não foram coletados dados além do nome, preenche-se os demais dados com texto vazio.
def gravar_dados_csv(dados_dispositivo):
    while len(dados_dispositivo) < 26:
        dados_dispositivo = dados_dispositivo + ['']

    with open('dados_coletados.csv', 'a', newline='') as arquivo_csv:
        writer = csv.writer(arquivo_csv)
        writer.writerow(dados_dispositivo)
    arquivo_csv.close()

#Verificar os dados coletados em cada site e decidir qual conjunto é considerado ou se é feito o merge de ambos
#Realiza a escrita no arquivo texto de log, de acordo com o resultado obtido
def selecionar_dados(dados_coletados_site1, dados_coletados_site2):
    #Quando não é possível coletar de nenhum site, salva-se apenas o nome e handset e informa o ocorrido no log
    if len(dados_coletados_site1) == 3 and len(dados_coletados_site2) == 3:
        arquivo = open("log_coleta_dispositivos.txt", "a")
        arquivo.write(dados_coletados_site1['handset'] + ' - ' + dados_coletados_site1['nome'] + " - " + "Não foi possível coletar os dados para este dispositivo nos sites " + dados_coletados_site1['site'] + ' e ' +  dados_coletados_site2['site'] + '\n')
        arquivo.close()
        del dados_coletados_site1['site']
        dados_coletados_site1 = list(dados_coletados_site1.values())
        gravar_dados_csv(dados_coletados_site1)
    
    #Quando os dados são coletados apenas do primeiro site, salva-se estes e informa o ocorrido no log
    elif len(dados_coletados_site1) > 3 and len(dados_coletados_site2) == 3:
        arquivo = open("log_coleta_dispositivos.txt", "a")
        arquivo.write(dados_coletados_site1['handset'] + ' - ' + dados_coletados_site1['nome'] + " - " + "Dados coletados do site " + dados_coletados_site1['site'] + ', pois não foi possível encontrar o dispositivo no site ' +  dados_coletados_site2['site'] + '\n')
        arquivo.close()
        del dados_coletados_site1['site']
        dados_coletados_site1 = list(dados_coletados_site1.values())
        gravar_dados_csv(dados_coletados_site1)

    #Quando os dados são coletados apenas do segundo site, salva-se estes e informa o ocorrido no log
    elif len(dados_coletados_site1) == 3 and len(dados_coletados_site2) > 3:
        arquivo = open("log_coleta_dispositivos.txt", "a")
        arquivo.write(dados_coletados_site2['handset'] + ' - ' + dados_coletados_site2['nome'] + " - " + "Dados coletados do site " + dados_coletados_site2['site'] + ', pois não foi possível encontrar o dispositivo no site ' +  dados_coletados_site1['site'] + '\n')
        arquivo.close()
        del dados_coletados_site2['site']
        dados_coletados_site2 = list(dados_coletados_site2.values())
        gravar_dados_csv(dados_coletados_site2)

    #Se há vários dados coletados de ambos os sites, verifica se são equivalentes
    else:
        site1 = dados_coletados_site1['site']
        site2 = dados_coletados_site2['site']
        del dados_coletados_site1['site']
        del dados_coletados_site2['site']
        dados_sao_equivalentes = True
        dados_resultantes = {}
        #As chaves a serem desconsideradas na comparação são aquelas que não correspondem a uma característica específica do dispositivo
        #Nome e HAndset são sempre colocados como os mesmos, já o preço pode variar de um site pra outro, por padrão escolhe-se o do primeiro site
        chaves_desconsideradas = ['handset', "nome", "url_dispositivo" , "data_hoje" , "preco", "avaliacao_site", "avaliacao_usuarios", "dimensoes_tela", "peso"]
        for chave in dados_coletados_site1.keys():
            if chave not in chaves_desconsideradas:
                if dados_coletados_site1[chave] == '' or dados_coletados_site2[chave] == '':
                    pass
                elif dados_coletados_site1[chave] != dados_coletados_site2[chave]:
                    dados_sao_equivalentes = False
                    break
        
        #Caso todos os dados considerados na comparação sejam iguais, fazz a união dos dados coletados de cada site
        #No caso de um campo vindo de um site ser vazio, então será substituído pelo do outro site. O preço é pego do primeiro site, caso esteja vazio, pega-se do outro site
        if dados_sao_equivalentes:
            for chave in dados_coletados_site1.keys():
                if (dados_coletados_site1[chave] != '' and dados_coletados_site2[chave] != '') or (dados_coletados_site1[chave] == '' and dados_coletados_site2[chave] == ''):
                    dados_resultantes[chave] = dados_coletados_site1[chave]
                
                elif dados_coletados_site1[chave] == '':
                    dados_resultantes[chave] = dados_coletados_site2[chave]
                
                elif dados_coletados_site2[chave] == '':
                    dados_resultantes[chave] = dados_coletados_site1[chave]
            
            if dados_coletados_site1['preco'] != '':
                dados_resultantes['preco'] = dados_coletados_site1['preco']
                dados_resultantes['url_dispositivo'] = dados_coletados_site1['url_dispositivo']
            elif dados_coletados_site2['preco'] != '':
                dados_resultantes['preco'] = dados_coletados_site2['preco']
                dados_resultantes['url_dispositivo'] = dados_coletados_site2['url_dispositivo']

        
            #Gravando dados em CSV e gerando log, no caso de equivalência dos dados
            arquivo = open("log_coleta_dispositivos.txt", "a")
            arquivo.write(dados_resultantes['handset'] + ' - ' + dados_resultantes['nome'] + " - " + "Feito o merge dos dados coletados dos sites " + site1 + " e " + site2 + ", pois os dados são todos equivalentes. Campos em branco foram unidos" + '\n')
            arquivo.close()
            dados_resultantes = list(dados_resultantes.values())
            gravar_dados_csv(dados_resultantes)

        #Dados não são equivalentes, logo verifica-se qual conjunto será usado
        else:
            #Prioriza-se aquele site cujos dados possuem o preço do dispositivo, então grava-se os dados deste em CSV e gera-se o log
            if dados_coletados_site1['preco'] != '' and dados_coletados_site2['preco'] == '':
                arquivo = open("log_coleta_dispositivos.txt", "a")
                arquivo.write(dados_coletados_site1['handset'] + ' - ' + dados_coletados_site1['nome'] + " - " + "Dados coletados do site " + site1 + ", pois os dados coletados dele e do site " + site2 + " não são equivalentes, de modo que este último não possui o preço, então priorizou-se o primeiro site." + '\n')
                arquivo.close()
                dados_coletados_site1 = list(dados_coletados_site1.values())
                gravar_dados_csv(dados_coletados_site1)
            
            elif  dados_coletados_site1['preco'] == '' and dados_coletados_site2['preco'] != '':
                arquivo = open("log_coleta_dispositivos.txt", "a")
                arquivo.write(dados_coletados_site2['handset'] + ' - ' + dados_coletados_site2['nome'] + " - " + "Dados coletados do site " + site2 + ", pois os dados coletados dele e do site " + site1 + " não são equivalentes, de modo que este último não possui o preço, então priorizou-se o primeiro site." + '\n')
                arquivo.close()
                dados_coletados_site2 = list(dados_coletados_site2.values())
                gravar_dados_csv(dados_coletados_site2)

            #Se ambos possuem o preço, prioriza-se aquele site que possui mais dados preenchidos - Não nulos - então grava-se os dados deste em CSV e gera-se o log
            else:
                dados_preenchidos_site1 = 0
                dados_preenchidos_site2 = 0
                for chave in dados_coletados_site1.keys():
                    if dados_coletados_site1[chave] != '':
                        dados_preenchidos_site1 = dados_preenchidos_site1 + 1
                    if dados_coletados_site2[chave] != '':
                        dados_preenchidos_site2 = dados_preenchidos_site2 + 1

                if dados_preenchidos_site1 > dados_preenchidos_site2:
                    arquivo = open("log_coleta_dispositivos.txt", "a")
                    arquivo.write(dados_coletados_site1['handset'] + ' - ' + dados_coletados_site1['nome'] + " - " + "Dados coletados do site " + site1 + ", pois os dados coletados dele e do site " + site2 + " não são equivalentes, de modo que este último possui menos dados preenchidos, então priorizou-se o primeiro site. Ambos possuem o preço." + '\n')
                    arquivo.close()
                    dados_coletados_site1 = list(dados_coletados_site1.values())
                    gravar_dados_csv(dados_coletados_site1)
                elif dados_preenchidos_site2 > dados_preenchidos_site1:
                    arquivo = open("log_coleta_dispositivos.txt", "a")
                    arquivo.write(dados_coletados_site2['handset'] + ' - ' + dados_coletados_site2['nome'] + " - " + "Dados coletados do site " + site2 + ", pois os dados coletados dele e do site " + site1 + " não são equivalentes, de modo que ste último possui menos dados preenchidos, então priorizou-se o primeiro site. Ambos possuem o preço." + '\n')
                    arquivo.close()
                    dados_coletados_site2 = list(dados_coletados_site2.values())
                    gravar_dados_csv(dados_coletados_site2)
                #Se ambos também possuem a mesma quantidade de dados preenchidos, escolhe-se qualquer um dos sites. Por padrão foi definido o primeiro, então grava-se os dados deste em CSV e gera-se o log
                else:
                    arquivo = open("log_coleta_dispositivos.txt", "a")
                    arquivo.write(dados_coletados_site1['handset'] + ' - ' + dados_coletados_site1['nome'] + " - " + "Dados coletados do site " + site1 + ", pois os dados coletados dele e do site " + site2 + " não são equivalentes, de modo que ambos possuem o preço e a mesma quantidade de dados preenchidos. Foi escolhido o primeiro apenas por opção." + '\n')
                    arquivo.close()
                    dados_coletados_site1 = list(dados_coletados_site1.values())
                    gravar_dados_csv(dados_coletados_site1)

