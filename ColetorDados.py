from selenium import webdriver
import TratamentoDados
import BuscaSites

dados_dispositivos = TratamentoDados.ler_entrada()
nomes_comerciais = dados_dispositivos['nome comercial']
handset = dados_dispositivos['handset']
lista_dispositivo = []
x = 0

firefox = webdriver.Firefox()

#Coletrando dados em cada site e salvando em dicionário
for nome in nomes_comerciais:
    dicionario_kimovi = {'handset' :  handset[x], 'nome' : nome, 'site': 'kimovil'}
    dicionario_zoom = {'handset' :  handset[x], 'nome' : nome, 'site' : 'zoom'}
    #Coletando do kimovil
    dados_coletados_kimovil = BuscaSites.buscar_dados_kimovil(firefox, nome)
    if dados_coletados_kimovil is not None:
        dicionario_kimovi.update(dados_coletados_kimovil)
    print("Dados do kimovil")
    print(dicionario_kimovi)
    #Coletando do zoom
    dados_coletados_zoom = BuscaSites.buscar_dados_zoom(firefox, 'smartphone ' + nome)
    if dados_coletados_zoom is not None:
        dicionario_zoom.update(dados_coletados_zoom)
    print("Dados do zoom")
    print(dicionario_zoom)
    x = x + 1

    TratamentoDados.selecionar_dados(dicionario_kimovi, dicionario_zoom)
    

firefox.quit()
