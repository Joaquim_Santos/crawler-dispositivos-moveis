from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
import requests
from bs4 import BeautifulSoup
from datetime import date
import re
import time

#Pesquisar dispositivo kimovil
def buscar_dados_kimovil(firefox, nome_dispositivo):
    try:
        firefox.get('https://www.kimovil.com/pt/')
    except TimeoutException:
            print("Excedido tempo de espera para carregamento da página do KIMOVIL, conexão de Internet ruim!!!")
            return None
    #Verificar se a página principal é carregada
    try:
        elemento = WebDriverWait(firefox, 10).until(EC.presence_of_element_located((By.ID, 'js_global-search-input')))
        barra_pesquisa = firefox.find_element_by_name('q')
        time.sleep(2)
        barra_pesquisa.send_keys(nome_dispositivo)
        barra_pesquisa.send_keys(Keys.ENTER)
        #Verificar se o dispositivo pesquisado foi encontrado
        try:
            elemento = WebDriverWait(firefox, 10).until(EC.presence_of_element_located((By.CLASS_NAME, 'device-name')))
            dispositivo = firefox.find_elements_by_class_name('device-name')[0]
            dispositivo.click()
            dados_coletados = extrair_dados_kimovil(firefox)
            return dados_coletados
        except TimeoutException:
            print("Excedido tempo de espera para carregamento da página do KIMOVIL após pesquisar nome do dispositivo!!!")
            return None
    
    except TimeoutException:
        print("Excedido tempo de espera para carregamento da página principal do KIMOVIL!!!")
        return None


#Extrair dados da página do disositivo acessada no site
def extrair_dados_kimovil(firefox):
    #Verificar se dados do dispositivo carregaram
    try:
        elemento = WebDriverWait(firefox, 10).until(EC.presence_of_element_located((By.CLASS_NAME, 'k-dl')))
        url_dispositivo = firefox.current_url
    except TimeoutException:
        print("Excedido tempo de espera para carregamento da página do KIMOVIL após clicar no link do dispositivo!!!")
        url_dispositivo = ''
    
    if url_dispositivo != '':
        marca = nfc = preco = resolucao_camera_traseira = resolucao_camera_frontal = versao_bluetooth = tipo_processador = ''
        data_hoje = date.today()
        data_hoje = data_hoje.strftime("%d/%m/%Y")
        #Requisição do conteúdo da página para beautiful soup
        page = requests.get(url_dispositivo)
        soup = BeautifulSoup(page.text, 'html.parser')
        #Obtendo a lista de dados do dispositivo pela classe html e percorrendo cada item
        lista_dados = soup.find_all("dl", class_='k-dl')
        for x in range(0, len(lista_dados)):
            try:
                 dado = str(lista_dados[x].find("dt").text).replace("\n", '').lower()
                 if dado == 'marca':
                     marca = str(lista_dados[x].find("dd").text).replace("\n", '')
                 
                 elif dado == 'capacidade'and len(lista_dados[x].find_all("dd")) > 2:
                     capacidade_bateria = str(lista_dados[x].find("dd").text).replace("\n", '')

                 elif dado =='ram':
                     memoria_ram = str(lista_dados[x].find("dd").text).replace("\n", '')
                
                 elif dado == 'capacidade'and len(lista_dados[x].find_all("dd")) <= 2:
                     memoria_armazenamento = str(lista_dados[x].find("dd").text).replace("\n", '')
                 
                 elif dado =='versão':
                     versao_bluetooth = str(lista_dados[x].find("dd").text).replace('\n', '')
                
                 elif dado =='nfc':
                     nfc = str(lista_dados[x].find("dd").text).replace("\n", '')
                
                 elif dado =='tipo':
                     dual_chip = str(lista_dados[x].find("dd").text).replace("\n", '')

                 elif dado =='4g lte':
                     lte = str(lista_dados[x].find("dd").text).replace("\n", '')
                 
                 elif dado ==' principal':
                     if resolucao_camera_traseira == '':
                         resolucao_camera_traseira = str(lista_dados[x].find_all("dd")[1].text).replace("\n", '')
                     else:
                         resolucao_camera_frontal = str(lista_dados[x].find_all("dd")[1].text).replace("\n", '')

                 elif dado =='tamanho':
                     dimensoes = str(lista_dados[x].find_all("dd")[0].text).replace("\n", '')
                     peso = str(lista_dados[x].find_all("dd")[1].text).replace("\n", '')

                 elif dado =='diagonal':
                     tela = str(lista_dados[x].find("dd").text).replace("\n", '')

                 elif dado =='sistema operativo':
                     sistema_operacional = str(lista_dados[x].find("dd").text).replace("\n", '')

                 elif dado =='modelo':
                     tipo_processador = str(lista_dados[x].find_all("dd")[2].text).replace("\n", '')
                     velocidade_processamento = str(lista_dados[x].find_all("dd")[3].text).replace("\n", '')

                 elif dado =='lançamento':
                     ano_lancamento = str(lista_dados[x].find("dd").text).replace("\n", '')
            except:
                pass
        
        #Dados de avaliação do produto e preço pegos em tags separadas da lista anterior
        try:
            avaliacao_site = str(soup.find("span", class_='score').text).replace("\n", '')
            avaliacao_usuarios = str(soup.find("div", class_='score').text).replace("\n", '')
        except:
            avaliacao_usuarios = avaliacao_site = ''
        
        try:
            for nome in firefox.find_elements_by_class_name("sub"):
                try:
                    if str(nome.text)[1] == '$':
                        preco = str(nome.text).split(' ')[0]
                        preco = preco.replace('R$', 'BR').replace(',', '.')
                        break
                except:
                    pass
        except:
            pass
        
        #Realizando formatação dos dados e verificando se todos foram coletados, caso algum não seja, atribui vazio à variável
        try:
            capacidade_bateria = re.compile("[a-z]|[A-Z]").split(capacidade_bateria)[0].replace(' ', '').replace('.', '')
        except:
            capacidade_bateria = ''
        
        try:
            memoria_ram = re.compile("[a-z]|[A-Z]").split(memoria_ram)[0].replace(' ', '')
        except:
            memoria_ram = ''
        
        try:
            memoria_armazenamento = re.compile("[a-z]|[A-Z]").split(memoria_armazenamento)[0].replace(' ', '')
        except:
            memoria_armazenamento = ''

        try:
            versao_bluetooth = re.compile("[a-z]|[A-Z]").split(versao_bluetooth.split(' ')[1])[0].replace(' ', '')
        except:
            versao_bluetooth = ''
        
        try:
            if dual_chip.split(' ')[0].lower() == 'dual':
                dual_chip = 'Sim'
            else:
                dual_chip = 'Não'
        except:
            dual_chip = ''

        try:
            if lte != '':
                lte = 'Sim'
        except:
            lte = ''

        try:    
            resolucao_camera_traseira = re.compile("[a-z]|[A-Z]").split(resolucao_camera_traseira)[0].replace(' ', '')
            resolucao_camera_frontal = re.compile("[a-z]|[A-Z]").split(resolucao_camera_frontal)[0].replace(' ', '')
            if '.' not in resolucao_camera_frontal and resolucao_camera_frontal != '':
                resolucao_camera_frontal = resolucao_camera_frontal + '.0'
            if '.' not in resolucao_camera_traseira and resolucao_camera_traseira != '':
                resolucao_camera_traseira = resolucao_camera_traseira + '.0'
        except:
            resolucao_camera_frontal = resolucao_camera_traseira = ''
        
        dimensoes_tela = ''
        for digito in dimensoes.split(' '):
            try:
                dimensoes_tela = dimensoes_tela + str(float(digito)) + 'X'
            except:
                pass
        if dimensoes_tela != '':
            dimensoes_tela = dimensoes_tela[0:len(dimensoes_tela)-1]
        
        try:
            peso = re.compile("[a-z]|[A-Z]").split(peso)[0].replace(' ', '').replace(',', '.')
            if '.' not in peso:
                peso = peso + '.0'
            tela = tela.replace('\"', '')
            if '.' not in tela:
                tela = tela + '.0'
        except:
            peso = tela = ''
        
        try:
            sistema_operacional = sistema_operacional.split(' ')
            versao_so = re.compile("[a-z]|[A-Z]").split(sistema_operacional[1])[0]
            versao_so = float(versao_so)
            sistema_operacional = sistema_operacional[0]
        except:
            sistema_operacional = versao_so = ''

        try:
            velocidade_processamento = float(re.compile("[a-z]|[A-Z]").split(velocidade_processamento)[0].replace(' ', ''))*1000
        except:
            velocidade_processamento = ''

        meses = ['jan','fev', 'mar', 'abr', 'mai', 'jun', 'jul', 'ago', 'set', 'out', 'nov', 'dez']
        x = 0
        try:
            for mes in meses:
                x = x + 1
                if ano_lancamento.split(' ')[0].lower()[0:3] == mes:
                    if x <= 9:
                        x = '0' + str(x)
                    ano_lancamento = str(x) + '/' + ano_lancamento.split(' ')[1].replace(',', '')
                    break
        except:
            ano_lancamento = ''
        
        #Retornando dados coletados e formatados
        return {'marca' : marca, 'modelo' : '','capacidade_bateria' : capacidade_bateria, 'memoria_ram' : memoria_ram, 'memoria_armazenamento' : memoria_armazenamento, 
                'versao_bluetooth' : versao_bluetooth, 'nfc' : nfc, 'dual_chip' : dual_chip, 'lte' : lte, 'resolucao_camera_traseira' : resolucao_camera_traseira, 
                'resolucao_camera_frontal' : resolucao_camera_frontal, 'dimensoes_tela' : dimensoes_tela, 'peso' : peso, 'polegadas' : tela, 
                'sistema_operacional' : sistema_operacional, 'versao_so' : versao_so, 'tipo_processador' : tipo_processador, 
                'velocidade_processamento' : velocidade_processamento, 'url_dispositivo' : url_dispositivo,
                'data_hoje' : data_hoje, 'ano_lancamento' : ano_lancamento, 'preco' : preco, 'avaliacao_site' : avaliacao_site, 'avaliacao_usuarios' : avaliacao_usuarios}

    else:
        return None


#Pesquisar dispositivo zoom
def buscar_dados_zoom(firefox, nome_dispositivo):
    try:
        firefox.get('https://www.zoom.com.br/celular/')
    except TimeoutException:
            print("Excedido tempo de espera para carregamento da página do zoom, conexão de Internet ruim!!!")
            return None
    #Verificar se a página principal é carregada
    try:
        elemento = WebDriverWait(firefox, 10).until(EC.presence_of_element_located((By.CLASS_NAME, 'zsearch__input')))
        barra_pesquisa = firefox.find_element_by_name('q')
        barra_pesquisa.send_keys(nome_dispositivo)
        barra_pesquisa.send_keys(Keys.ENTER)
        #Verificar se o dispositivo pesquisado foi encontrado, conferindo em qual página foi e chamando a função de extração para a página em específico
        #Verficar se foi encontrado, primeiro, na página com a lista de dispositivos 
        try:
            elemento = WebDriverWait(firefox, 10).until(EC.presence_of_element_located((By.CLASS_NAME, 'prod-name')))
            dados_coletados = extrair_dados_zoom(firefox, 1, nome_dispositivo)
            return dados_coletados
        except TimeoutException:
            #Verificar se foi encontrado na página já com os dados do dispositivo
            try:
                elemento = WebDriverWait(firefox, 10).until(EC.presence_of_element_located((By.CLASS_NAME, 'ti')))
                dados_coletados = extrair_dados_zoom(firefox, 2, nome_dispositivo)
                return dados_coletados
            except TimeoutException:
                print("Excedido tempo de espera para carregamento da página do ZOOM após pesquisar nome do dispositivo!!!")
                return None
    
    except TimeoutException:
        print("Excedido tempo de espera para carregamento da página principal do ZOOM!!!")
        return None


def extrair_dados_zoom(firefox, pagina, nome_dispositivo):
    #Verificar se o dispositivo está na lista obtida
    if pagina == 1:
        encontrou = False
        #Requisição do conteúdo da página para beautiful soup
        page = requests.get(firefox.current_url)
        soup = BeautifulSoup(page.text, 'html.parser')
        dispositivo = soup.find("h2",  class_ = "prod-name")

        #Verifica se o dispositivo da lista é da própria loja do zoom, dispositivos com link para outra loja são ignorados
        try:
            url_dispositivo = dispositivo.find('a')['href']
            if (str(url_dispositivo.split('/')[1]) == 'celular'):
                firefox.get('https://www.zoom.com.br' + url_dispositivo)
                encontrou = True
        except:
            pass
            
        if encontrou:
            try:
                elemento = WebDriverWait(firefox, 10).until(EC.presence_of_element_located((By.CLASS_NAME, 'ti')))
                dados_coletados = extrair_dados_zoom(firefox, 2, nome_dispositivo)
                return dados_coletados
            except TimeoutException:
                print("Excedido tempo de espera para carregamento da página do ZOOM após pesquisar nome do dispositivo!!!")
                return None
        else:
            print("Dispositivo não encontrado no próprio site do ZOOM!!!")
            return None

    #Iniciar coleta de dados na página do dispositivo   
    else:
        marca = modelo = capacidade_bateria = memoria_ram = memoria_interna = dual_chip = nfc = resolucao_camera_frontal = ''
        resolucao_camera_traseira = peso = dimensoes = tela = sistema_operacional = versao_so = velocidade_processamento = ''
        tipo_processador = preco = ''
        data_hoje = date.today()
        data_hoje = data_hoje.strftime("%d/%m/%Y")
        url_dispositivo = firefox.current_url
        #Requisição do conteúdo da página para beautiful soup
        page = requests.get(url_dispositivo)
        soup = BeautifulSoup(page.text, 'html.parser')
        #Obtendo a lista de dados do dispositivo pela classe html e percorrendo cada item
        lista_dados = soup.find_all("tr", class_='ti')
        for x in range(0, len(lista_dados)):
            try:
                dado = str(lista_dados[x].find("td").text).replace(" ", '').lower()
                if dado == 'marca':
                    marca = str(lista_dados[x].find_all("td")[1].text)

                elif dado == 'modelo':
                    modelo = str(lista_dados[x].find_all("td")[1].text)
                     
                elif dado == 'capacidadedabateria':
                    capacidade_bateria = str(lista_dados[x].find_all("td")[1].text)
                    capacidade_bateria = re.compile("[a-z]|[A-Z]").split(capacidade_bateria)[0].replace(' ', '').replace('.', '')

                elif dado == 'memóriaram':
                    memoria_ram = str(lista_dados[x].find_all("td")[1].text)
                    memoria_ram = re.compile("[a-z]|[A-Z]").split(memoria_ram)[0].replace(' ', '')

                elif dado[0:14] == 'memóriainterna':
                    memoria_interna = str(lista_dados[x].find_all("td")[1].text)
                    memoria_interna = re.compile("[a-z]|[A-Z]").split(memoria_interna)[0].replace(' ', '')
                
                elif dado[0:14] == 'tipodeaparelho':
                    itens = soup.find_all("span", class_='item')
                    for item in itens:
                        if(str(item.text) == '2 Chips'):
                            dual_chip = 'Sim'
                            break
                        else:
                            dual_chip = 'Não'

                elif dado[0:14] == 'conectividade':
                    nfc = 'Não'
                    itens = str(lista_dados[x].find_all("td")[1].text)
                    for item in itens.split(' '):
                        if str(item).lower() == 'nfc':
                            nfc = 'Sim'
                            break

                elif dado == 'resoluçãodacâmerafrontal':
                    resolucao_camera_frontal = str(lista_dados[x].find_all("td")[1].text)
                    resolucao_camera_frontal = str(resolucao_camera_frontal.split(' ')[0])
                    if '.' not in resolucao_camera_frontal and resolucao_camera_frontal != '':
                        resolucao_camera_frontal = resolucao_camera_frontal + '.0'
                
                elif dado == 'resoluçãodacâmeratraseira':
                    resolucao_camera_traseira = str(lista_dados[x].find_all("td")[1].text)
                    resolucao_camera_traseira = str(resolucao_camera_traseira.split(' ')[0])
                    if '.' not in resolucao_camera_traseira and resolucao_camera_traseira != '':
                        resolucao_camera_traseira = resolucao_camera_traseira + '.0'

                elif dado == 'peso':
                    peso = str(lista_dados[x].find_all("td")[1].text)
                    peso = str(peso.split(' ')[0].replace(',', '.'))
                    if '.' not in peso:
                        peso = peso + '.0'
                
                elif dado == 'altura' or dado == 'largura' or dado == 'profundidade':
                    dimensoes = dimensoes + 'X' + str(lista_dados[x].find_all("td")[1].text).split(' ')[0]

                elif dado == 'tamanhodatela':
                    tela = str(lista_dados[x].find_all("td")[1].text)
                    tela = str(tela.split(' ')[0].replace(',', '.'))
                    if '.' not in tela:
                        tela = tela + '.0'
                
                elif dado[0:18] == 'sistemaoperacional':
                    sistema_operacional = str(lista_dados[x].find_all("td")[1].text)
                    versao_so = float(sistema_operacional.split(' ')[1])
                    sistema_operacional = sistema_operacional.split(' ')[0]

                elif dado == 'velocidadedoprocessador':
                    velocidade_processamento = str(lista_dados[x].find_all("td")[1].text)
                    velocidade_processamento = float(velocidade_processamento.split(' ')[0].replace(',', '.'))*1000
                
                elif dado[0:19] == 'núcleodeprocessador':
                    tipo_processador = str(lista_dados[x].find_all("td")[1].text).replace(' ', '')

            except:
                pass
        #Preço obtido separadamente da lista com os demais dados
        try:
            preco = str(soup.find("strong").text).replace(',', '.').split(' ')[2]
            preco = 'BR' + preco
        except:
            pass
        
        #Formatando string de dimensões da tela
        try:
            dimensoes = dimensoes[1:].replace(',', '.')
        except:
            pass

        #Retornando dados coletados e formatados
        return{'marca' : marca, 'modelo' : modelo, 'capacidade_bateria' : capacidade_bateria, 'memoria_ram' : memoria_ram, 'memoria_armazenamento' : memoria_interna, 
               'versao_bluetooth' : '', 'nfc' : nfc, 'dual_chip' : dual_chip, 'lte' : '', 'resolucao_camera_traseira' : resolucao_camera_traseira, 
               'resolucao_camera_frontal' : resolucao_camera_frontal, 'dimensoes_tela' : dimensoes, 'peso' : peso, 'polegadas' : tela, 
               'sistema_operacional' : sistema_operacional, 'versao_so' : versao_so,  'tipo_processador' : tipo_processador, 'velocidade_processamento' : velocidade_processamento,  
               'url_dispositivo' : url_dispositivo,  'data_hoje' : data_hoje, 'ano_lancamento' : '', 'preco' : preco, 'avaliacao_site' : '', 'avaliacao_usuarios' : ''}
